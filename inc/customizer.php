<?php
/**
 * Twenty Seventeen: Customizer
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function twentyseventeen_customize_register( $wp_customize ) {



	require get_parent_theme_file_path( '/inc/repeater-control.php' );
	require get_parent_theme_file_path( '/inc/controls/page-section/page-section-control.php' );
	$wp_customize->register_control_type( 'Laser_Page_Section_Control' );

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->selective_refresh->add_partial(
		'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'twentyseventeen_customize_partial_blogname',
		)
	);
	$wp_customize->selective_refresh->add_partial(
		'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'twentyseventeen_customize_partial_blogdescription',
		)
	);

	/**
	 * Custom colors.
	 */
	$wp_customize->add_setting(
		'colorscheme', array(
			'default'           => 'light',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'twentyseventeen_sanitize_colorscheme',
		)
	);

	$wp_customize->add_setting(
		'colorscheme_hue', array(
			'default'           => 250,
			'transport'         => 'postMessage',
			'sanitize_callback' => 'absint', // The hue is stored as a positive integer.
		)
	);

	$wp_customize->add_control(
		'colorscheme', array(
			'type'     => 'radio',
			'label'    => __( 'Color Scheme', 'twentyseventeen' ),
			'choices'  => array(
				'light'  => __( 'Light', 'twentyseventeen' ),
				'dark'   => __( 'Dark', 'twentyseventeen' ),
				'custom' => __( 'Custom', 'twentyseventeen' ),
			),
			'section'  => 'colors',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize, 'colorscheme_hue', array(
				'mode'     => 'hue',
				'section'  => 'colors',
				'priority' => 6,
			)
		)
	);

	/**
	 * Theme options.
	 */
	$wp_customize->add_section(
		'theme_options', array(
			'title'    => __( 'Theme Options', 'twentyseventeen' ),
			'priority' => 130, // Before Additional CSS.
		)
	);
	/**
	 * Theme options.
	 */
	$wp_customize->add_section(
		'laser_demo', array(
			'title'    => __( 'Laser Demo', 'twentyseventeen' ),
			'priority' => 10, // Before Additional CSS.
		)
	);

	$wp_customize->add_setting(
		'page_layout', array(
			'default'           => 'two-column',
			'sanitize_callback' => 'twentyseventeen_sanitize_page_layout',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'page_layout', array(
			'label'           => __( 'Page Layout', 'twentyseventeen' ),
			'section'         => 'theme_options',
			'type'            => 'radio',
			'description'     => __( 'When the two-column layout is assigned, the page title is in one column and content is in the other.', 'twentyseventeen' ),
			'choices'         => array(
				'one-column' => __( 'One Column', 'twentyseventeen' ),
				'two-column' => __( 'Two Column', 'twentyseventeen' ),
			),
			'active_callback' => 'twentyseventeen_is_view_with_layout_option',
		)
	);



	// Repeater
	
		// Add our Sortable Repeater setting and Custom Control for Social media URLs
		// $wp_customize->add_setting( 'sample_sortable_repeater_control',
		// 	array(
		// 		'default' => '',//$this->defaults['sample_sortable_repeater_control'],
		// 		'transport' => 'postMessage',
		// 		'sanitize_callback' => 'skyrocket_url_sanitization'
		// 	)
		// );
		// $wp_customize->add_control( new Skyrocket_Sortable_Repeater_Custom_Control( $wp_customize, 'sample_sortable_repeater_control',
		// 	array(
		// 		'label' => __( 'Sortable Repeater', 'skyrocket' ),
		// 		'description' => esc_html__( 'This is the control description.', 'skyrocket' ),
		// 		'section' => 'colors',
		// 		'button_labels' => array(
		// 			'add' => __( 'Add Row', 'skyrocket' ),
		// 		)
		// 	)
		// ) );


	// Repeater
	
		$wp_customize->add_setting( 'laser_page_section',
			array(
				'default' => '',//$this->defaults['laser_page_section'],
				'transport' => 'postMessage',
				'sanitize_callback' => 'skyrocket_url_sanitization'
			)
		);
		$wp_customize->add_control( new Laser_Page_Section_Control( $wp_customize, 'laser_page_section',
			array(
				'label' => __( 'Page sections test', 'laser' ),
				'description' => esc_html__( 'This is the control description.', 'laser' ),
				'section' => 'laser_demo',
				'button_labels' => array(
					'add' => __( 'Add Section', 'laser' ),
				)
			)
		) );


	$wp_customize->add_setting(
		'_laser_page_element_array', array(
			'default'           => false,
			// 'sanitize_callback' => 'twentyseventeen_sanitize_page_layout',
			'transport'         => 'postMessage',
		)
	);


	$wp_customize->add_control(
		'_laser_page_element_array', array(
			'type'     => 'text',
			'label'    => __( 'Text test', 'twentyseventeen' ),
			'section'  => 'laser_demo',
		)
	);


	$settings = get_theme_mod('_laser_page_element_array', null);

	if ( is_array($settings) ) {
		
		foreach ($settings as $setting ) {
			
			$st = json_decode( $setting, true );
			if ( is_array( $st ) ) {
					$wp_customize->add_setting(
						$st['id'], array(
							'default'           => $st['settings']['text'],
							// 'sanitize_callback' => 'twentyseventeen_sanitize_page_layout',
							'transport'         => 'postMessage',
						)
					);

					$wp_customize->add_control(
						$st['id'], array(
							'type'     => 'text',
							'label'    => __( 'Text test', 'twentyseventeen' ),
							'section'  => 'laser_demo',
						)
					);

			}
		}

	}


	/**
	 * Filter number of front page sections in Twenty Seventeen.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $num_sections Number of front page sections.
	 */
	$num_sections = apply_filters( 'twentyseventeen_front_page_sections', 4 );

	// Create a setting and control for each of the sections available in the theme.
	for ( $i = 1; $i < ( 1 + $num_sections ); $i++ ) {
		$wp_customize->add_setting(
			'panel_' . $i, array(
				'default'           => false,
				'sanitize_callback' => 'absint',
				'transport'         => 'postMessage',
			)
		);

		$wp_customize->add_control(
			'panel_' . $i, array(
				/* translators: %d is the front page section number */
				'label'           => sprintf( __( 'Front Page Section %d Content', 'twentyseventeen' ), $i ),
				'description'     => ( 1 !== $i ? '' : __( 'Select pages to feature in each area from the dropdowns. Add an image to a section by setting a featured image in the page editor. Empty sections will not be displayed.', 'twentyseventeen' ) ),
				'section'         => 'theme_options',
				'type'            => 'dropdown-pages',
				'allow_addition'  => true,
				'active_callback' => 'twentyseventeen_is_static_front_page',
			)
		);

		$wp_customize->selective_refresh->add_partial(
			'panel_' . $i, array(
				'selector'            => '#panel' . $i,
				'render_callback'     => 'twentyseventeen_front_page_section',
				'container_inclusive' => true,
			)
		);
	}
}
add_action( 'customize_register', 'twentyseventeen_customize_register' );

/**
 * Sanitize the page layout options.
 *
 * @param string $input Page layout.
 */
function twentyseventeen_sanitize_page_layout( $input ) {
	$valid = array(
		'one-column' => __( 'One Column', 'twentyseventeen' ),
		'two-column' => __( 'Two Column', 'twentyseventeen' ),
	);

	if ( array_key_exists( $input, $valid ) ) {
		return $input;
	}

	return '';
}

/**
 * Sanitize the colorscheme.
 *
 * @param string $input Color scheme.
 */
function twentyseventeen_sanitize_colorscheme( $input ) {
	$valid = array( 'light', 'dark', 'custom' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}

	return 'light';
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see twentyseventeen_customize_register()
 *
 * @return void
 */
function twentyseventeen_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see twentyseventeen_customize_register()
 *
 * @return void
 */
function twentyseventeen_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Return whether we're previewing the front page and it's a static page.
 */
function twentyseventeen_is_static_front_page() {
	return ( is_front_page() && ! is_home() );
}

/**
 * Return whether we're on a view that supports a one or two column layout.
 */
function twentyseventeen_is_view_with_layout_option() {
	// This option is available on all pages. It's also available on archives when there isn't a sidebar.
	return ( is_page() || ( is_archive() && ! is_active_sidebar( 'sidebar-1' ) ) );
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function twentyseventeen_customize_preview_js() {
	wp_enqueue_script( 'twentyseventeen-customize-preview', get_theme_file_uri( '/assets/js/customize-preview.js' ), array( 'customize-preview' ), '1.0', true );
}
add_action( 'customize_preview_init', 'twentyseventeen_customize_preview_js' );

/**
 * Load dynamic logic for the customizer controls area.
 */
function twentyseventeen_panels_js() {
	wp_enqueue_script( 'twentyseventeen-customize-controls', get_theme_file_uri( '/assets/js/customize-controls.js' ), array(), '1.0', true );
}
add_action( 'customize_controls_enqueue_scripts', 'twentyseventeen_panels_js' );







