
<?php 

	class Laser_Page_Section_Control extends WP_Customize_Control {
		/**
		 * The type of control being rendered
		 */
		public $type = 'page_section';
		/**
 		 * Button labels
 		 */
		public $button_labels = array();
		/**
		 * Constructor
		 */
		public function __construct( $manager, $id, $args = array(), $options = array() ) {
			parent::__construct( $manager, $id, $args );
			// Merge the passed button labels with our default labels
			$this->button_labels = wp_parse_args( $this->button_labels,
				array(
					'add' => __( 'Add', 'laser' ),
				)
			);
		}
		/**
		 * Enqueue our scripts and styles
		 */
		public function enqueue() {
			wp_enqueue_script( 'laser-page-section-controls-js', trailingslashit( get_template_directory_uri() ) . '/inc/controls/page-section/customizer.js', array( 'jquery', 'jquery-ui-core' ), '1.0', true );
			wp_enqueue_style( 'laser-page-section-controls-css', trailingslashit( get_template_directory_uri() ) . '/inc/controls/page-section/customizer.css', array(), '1.0', 'all' );
		}


	public function content_template(){
		?>
        <label>

            <# if ( data.label ) { #>
            <span class="customize-control-title">{{{ data.label }}}</span>
            <# } #>
            <# if ( data.description ) { #>
            <span class="description customize-control-description">{{{ data.description }}}</span>
            <# } #>
        </label>
        <input data-hidden-value type="hidden" {{{ data.inputAttrs }}} value="" {{{ data.link }}}/>
        <div class="laser-form-data" data-config="{{ JSON.stringify( data.default ) }}">
            <ul class="laser-list-repeater-sortable sortable-placeholder sortable-class">
					<button class="button customize-control-sortable-repeater-add" type="button"><?php echo $this->button_labels['add']; ?></button>
            </ul>

        </div>
		<?php
	}

	}
