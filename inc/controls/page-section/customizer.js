( function( api, $, _ ) {

	api.bind( 'ready', function() {
				var laser_elements = api( '_laser_page_element_array' );
				// var elm = JSON.parse(laser_elements.get());
				// var oldelm = api.settings.values._laser_page_element_array;
				var elm = {};


		$('.customize-control-sortable-repeater-add').click(function(event) {


				// Demo create dynamic setting


				var setting_id = '_laser_element_' + guid();
		  		var Setting = api.Setting.extend( {} );
				var _setting = api( setting_id );
	
				if ( ! _setting ) {
					_setting = new Setting( setting_id, 'text', {
						transport: 'postMessage',
						previewer: api.previewer,
						dirty: false
					} );
					api.add( setting_id, _setting );
					
					var control = new api.Control( setting_id, {
						    section: 'laser_demo',
						    type: 'text',
						    label: 'Text',
						    settings: { 
						    	default:setting_id,
						     },
						} );

					api.control.add( control );


					api( setting_id, function( value ) {	
						value.bind( function( to ) {

							var _newElement = {
								id:setting_id,
								settings:{
									text: to
								}
							}
							var sum = [];
							sum.push(_newElement);
							elm = $.parseJSON(laser_elements.get());

							if (null != elm ) {
								// elm.push(_newElement);
								sum.push(elm);
								// sum = $.extend(elm, _newElement);
							}else{
								// sum = _newElement;
							}

							laser_elements.set(JSON.stringify(sum));

							laser_elements.previewer.send( 'laser_elements', [ laser_elements.id, laser_elements() ] );

						});
					});


				
				}



		});


	})

})( wp.customize, jQuery, _ );

//@return a 24 digits global unique identifier
function guid() {
      function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
      }
      return s4() + s4() + s4() + s4() + s4() + s4();
}
